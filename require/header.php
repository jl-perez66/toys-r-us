<div class="contain-p">
<header>
        <a href="index.php"><img src="/img/logo.jpg"></a>
        <div class="navbar-main">
            <div class="navbar-menu">
                <ul>
                    <li><a href="list.php" class="all-toys">Tous les jouets</a></li>

                    <li>
                        <a href="list.php" class="per-mark"> Par marque <i class="fas fa-caret-down"></i></a>
                        
                        <ul class="marques">
                            <?php
                                require_once'require/models/list-brands.php';
                                $marques=ListBrands();
                                foreach ($marques as $marque) :
                            ?>
                            <li>
                                <a href="list.php?select-marque=<?php echo $marque['id'] ?>" class="mark-btn"><?php echo $marque['name']?></a></li>
                            
                            <?php
                        endforeach; ?>
                        </ul>
                        
                    </ul>
                </li>
            </div>
        </div>
    </header>
