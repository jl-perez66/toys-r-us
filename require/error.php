<div class="error">
            <span class="fa-stack fa-2x">
                <i class="fas fa-circle fa-stack-2x" style='color: #0056af' ></i>
                <i class="fas fa-exclamation-triangle fa-stack-1x fa-inverse" ></i>
            </span>
            <h1>Minute, papillon !</h1><br>
            <h3> Une erreur est survenue ! </h3> <br>
            <a href="index.php">Retourner à la page d'accueil</a>
        </div>