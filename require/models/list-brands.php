<?php
// FONCTION PERMETTANT DE RECUPERER LES MARQUES
function ListBrands(): array
{
    $arr_all = [];
   
    $mysql = databaseConnection();



    $q_all = 'SELECT * FROM brands';


    $r_all = mysqli_query($mysql,$q_all);


    databaseClose();
 

    if( ! $r_all){
  

        return $arr_all;
    }


    while($marque=mysqli_fetch_assoc($r_all) ){
        
        $arr_all[]=$marque;
    }
    return $arr_all;
}
