<?php
// FONCTION PERMETTANT DE RECUPERRER LES VENTES (POUR TOP 3 -> INDEX.PHP) //
function ListSales():array 
{
    $arr_all = [];

    $mysql = databaseConnection();
    
    $q_all = 'SELECT SUM(quantity) as somme,
     toy_id,
      NAME,
       price,
        image FROM sales INNER JOIN toys ON sales.toy_id = toys.id GROUP BY toy_id ORDER BY somme DESC LIMIT 3 -- 
    ';

    $r_all = mysqli_query($mysql, $q_all);

    databaseClose();

    if (! $r_all){
        return $arr_all;
    }
    while ($ventes = mysqli_fetch_assoc($r_all) ){
        $arr_all[] = $ventes;
    }
    return $arr_all;
}