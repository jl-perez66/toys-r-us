<?php
// Query n°1 : récupère tout de toys ; Query n°2 : récupère tout de toys où brand_id = à $brand
function ListModelAll(): array
{
    $arr_all=[];
    $mysql = databaseConnection();
    $q_all = 'SELECT *  FROM toys';
    $r_all = mysqli_query ($mysql,$q_all);

    databaseClose();

    if( ! $r_all){
        return $arr_all;
    }
    while ($jouet=mysqli_fetch_assoc($r_all) ) {
        $arr_all[]= $jouet;
    }

    return $arr_all;
}

function ListModelByBrands($brand): array
{
    $arr_all=[];
    $mysql = databaseConnection();
    $q_all = 'SELECT *  FROM toys WHERE brand_id = ' . $brand;
    $r_all = mysqli_query ($mysql,$q_all);

    databaseClose();

    if( ! $r_all){
        return $arr_all;
    }
    while ($jouet=mysqli_fetch_assoc($r_all) ) {
        $arr_all[]= $jouet;
    }

    return $arr_all;
}

function ListModelById($toy_id): ?array{
    $mysql = databaseConnection();
    $q_all = 'SELECT 
                toys.*,
                brands.name as brand,
	            SUM( stock.quantity ) AS stock 
            FROM toys 
                JOIN brands ON brands.id = toys.brand_id
                JOIN stock ON stock.toy_id = toys.id
            WHERE toys.id = '. $toy_id . '
            GROUP BY toys.id';
    $r_all = mysqli_query ($mysql,$q_all);

    databaseClose();
    if ($r_all){
        return mysqli_fetch_assoc($r_all);
    }
    else {
        return null;
    }

}
