<?php 
// FONCTION PERMETTANT DE RECUPERER LES MAGASINS AVEC ID
function ListStores(): array{
$arr_all = [];
$mysql = databaseConnection();
$q_all = 'SELECT id AS id_magasin, name AS name_magasin, postal_code, city  FROM stores';
$r_all = mysqli_query($mysql, $q_all);

databaseClose();

     if (!$r_all){
         return $arr_all;
     }
     while ($magasin=mysqli_fetch_assoc($r_all) ){
         $arr_all[] = $magasin; 
     }
     return $arr_all;
}