<?php
// FONCTION PERMETTANT DE RECUPERER LES MAGASINS 
function ListStock($toy_id, $store_id): string{

$mysql= databaseConnection();
$q_all= 'SELECT quantity FROM stock WHERE toy_id = ' . $toy_id .' AND store_id = ' . $store_id;
$r_all = mysqli_query($mysql, $q_all);

databaseClose();


if( ! $r_all){
    return '';
}

$stock=mysqli_fetch_assoc($r_all);
return $stock['quantity'];

}