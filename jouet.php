<!-- DECLARATION DE VARIABLE + REQUIRE -->
<?php 
    require_once 'require/models/list-model.php';
    require_once 'require/models/list-stock.php';
    require_once 'require/models/list-stores.php';
    require_once 'require/req-principal.php';
    // Si le paramètre $_GET['jouet] est vide, alors $toy_id retourne -1, sinon, $toy_id retourne le paramètre de $_GET['jouet']
    $toy_id = empty($_GET['jouet']) ? -1 : $_GET['jouet'];
    // Si $toy_id est inférieur à 0, (dans le cas où il retourne -1), $detail ne retourne rien, sinon, on lui applique la fonction ListModelById() avec comme paramètre, le $_GET['jouet']
    // qu'on a stocké dans la variable $toy_id
    $detail = $toy_id < 0 ? null : ListModelById($toy_id);
    
?>

<!-- HTML COMMENCE ICI -->
<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <!-- On require le head -->
    <?php require_once 'require/req-head.php'?>
    <!-- Si $detail renvoie null, nous affichons en string 'Oups' sinon, on va chercher le nom dans le tableau de ListModelById()-->
    <title><?php echo is_null($detail) ? 'Oups !' : $detail['name']?></title>
</head>
<body>
    <?php ;
// Si le $detail renvoie null ($toy_id < 0), nous affichons error.php qui est un message d'erreur
/** NOTA BENE :
 * Si l'ID posté dans $_GET est introuvable (Il n'est pas null mais est != des ID dans la), c'est le résultat de mysqli_fetch_assoc qui renvoie null
 */
    if (is_null($detail) ): 
    require_once'require/error.php'?>
        
        <?php else :
            $magasins = ListStores(); 
            $stock = empty($_POST['select-magasin']) ? $detail['stock'] : ListStock($detail['id'],$_POST['select-magasin']);
        ?>
                <h1 class="title"><?php echo $detail['name'] ?></h1>
            <div class="jouet-description">
                <div class="jouet-description-left">
                    <img src="img/<?php echo $detail['image'];?>">
                    <p class="price"><?php echo str_replace('.' , ',', $detail['price']) ?>€</p>
                    <form method="POST">
                        <select class="select-magasin" name="select-magasin">
                            <option value = ''>Magasin</option>
                            <?php foreach ($magasins as $magasin):
                                $selected = '';
                                if (!empty($_POST['select-magasin']) && $_POST['select-magasin'] == $magasin['id_magasin']){
                                    $selected = ' selected=selected';
                                }
                            ?>
                            <option value = '<?php echo $magasin['id_magasin']?>' <?php echo $selected?>><?php echo $magasin['name_magasin']?> - <?php echo $magasin['postal_code']?>  <?php echo $magasin['city']?></option> 
                        <?php endforeach ?>
                        </select> 
                        <input type="submit" class="submit">
                    </form>
                    <p class="para-principal"> <span class="para-blue"> Stock :</span> <?php echo $stock?></p>
                </div>
                <div class="jouet-description-right">
                    <p class ="para-principal"><span class="para-blue">Marque :</span> <?php echo $detail['brand'] ?></p>
                   <?php echo $detail['description']?>
                </div>
            </div>
    <?php endif ?>
</body>
</html>