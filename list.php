<!DOCTYPE html>
<html lang="en">
<head>
    
    <?php require_once'require/req-head.php'?>
    <title>Liste de nos jouets</title>
</head>
<body>
    <?php require_once'require/req-principal.php';?>
    <div class="selecteur">
        <h1>Les jouets </h1>
        <form method="GET">
            <select name="select-marque">
                <option value="all">Toutes les marques</option>
                <?php 
                foreach ($marques as $marque) :
                    $selected = '';
                if(!empty($_GET["select-marque"])&&$_GET["select-marque"]==$marque['id'])
                {
                    $selected = ' selected="selected"';
                }
                ?>
                <option value="<?php echo $marque['id']?>"<?php echo $selected?>><?php echo $marque['name']?></option>
                <?php endforeach?>
                <input type="submit">
            </select>       
        </form>
    </div>
    <div class="container-list">
        <?php
            require_once'require/models/list-model.php';
        
            $jouets = empty($_GET["select-marque"]) || $_GET["select-marque"] == 'all' ? ListModelAll() : ListModelByBrands($_GET['select-marque']) ;
            foreach ($jouets as $jouet): 
        ?>
        <div class="jouet"> 
                <a href="jouet.php?jouet=<?php echo $jouet['id']?>">
                    <span class="img"><img src="<?php echo 'img/'. $jouet['image']?>"></span>
                    <p class="title-toy"><?php echo $jouet['name'] ?></p>
                    <p class="price"><?php echo str_replace('.',',',$jouet['price'])?>€</p>
                </a>
        </div>
            <?php endforeach ?>
    </div>

    </div>
    
</body>
</html>