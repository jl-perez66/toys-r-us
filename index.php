 <!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once'require/req-head.php' ?>
    <title>Toys'R'Us</title>
</head>
<body>
<?php
require_once'require/req-principal.php';
require_once'require/models/list-sales.php';
?>

<div class="top-3">
    <div class="title">
        <h1>Top 3 des Ventes </h1>
    </div>
    <div class="container-list">
    <?php $jouets = ListSales();
    foreach ($jouets as $jouet) : ?>
        <div class="jouet"> 
           <a href="jouet.php?jouet=<?php echo $jouet['toy_id']?>">
            <span class="img"><img src="<?php echo 'img/'. $jouet['image']?>"></span>
            <p class="title-toy"><?php echo $jouet['NAME'] ?></p>
            <p><span class="price"><?php echo str_replace('.',',',$jouet['price'])?>€</p></span>
           </a>
        </div>
    <?php endforeach ?>
</body>
</html>
